
## 0.4.0 [05-25-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-service_desk_plus!8

---

## 0.3.1 [07-09-2021]

- Made change to the Change calls - added OPERATION_NAME where it was missing and fix some paths that were incorrect. Testing on POC

See merge request itentialopensource/adapters/itsm-testing/adapter-service_desk_plus!7

---

## 0.3.0 [06-30-2021]

- Adding about 14 calls to the adapter -
  - getRequests
  - getRequestById
  - getRequestSummary
  - associateProblemWithRequest
  - dissociateProblemWithRequest
  - getProblemsFromRequest
  - getResolutionsFromRequest
  - assignRequest
  - closeRequest
  - getChanges
  - getChangeById
  - createChange
  - updateChange
  - deleteChange

See merge request itentialopensource/adapters/itsm-testing/adapter-service_desk_plus!6

---

## 0.2.3 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/itsm-testing/adapter-service_desk_plus!5

---

## 0.2.2 [07-10-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-service_desk_plus!4

---

## 0.2.1 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-service_desk_plus!3

---

## 0.2.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/itsm-testing/adapter-service_desk_plus!2

---

## 0.1.1 [11-04-2019]

- Initial Commit

See commit 12487cd

---
