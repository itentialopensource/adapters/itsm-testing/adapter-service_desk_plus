# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Service_desk_plus System. The API that was used to build the adapter for Service_desk_plus is usually available in the report directory of this adapter. The adapter utilizes the Service_desk_plus API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The ServiceDesk Plus adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ServiceDesk Plus. 
With this adapter you have the ability to perform operations with ServiceDesk Plus on items such as:

- Change
- Problem
- Request

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
