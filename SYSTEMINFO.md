# Adapter for ServiceDesk Plus

Vendor: ManageEngine
Homepage: https://www.manageengine.com

Product: ServiceDesk Plus
Product Page: https://www.manageengine.com/products/service-desk/

## Introduction
We classify ServiceDesk Plus into the ITSM or Service Management domain as ServiceDesk Plus provides methods for automating IT service management functions, facilitating the creation, updating, and retrieval of help desk tickets, assets, and other IT resources

## Why Integrate
The ServiceDesk Plus adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ServiceDesk Plus. 
With this adapter you have the ability to perform operations with ServiceDesk Plus on items such as:

- Change
- Problem
- Request

## Additional Product Documentation
[API documents for ServiceDesk Plus](https://www.manageengine.com/products/service-desk/sdpop-v3-api/)